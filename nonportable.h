/*
 * Copyright 2020 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _EO_NON_PORTABLE_H
#define _EO_NON_PORTABLE_H

#ifdef __cplusplus
extern "C" {  // only need to export C interface if
              // used by C++ source code
#endif

extern EO_EVENT_P eo_timeout_event;

//* this macro must implement all the code needed to enter a critical section
#define DI()
//* this macro must implement all the code needed to exit a critical section
#define EI()

//* All the power status available in the processor
enum eo_power_status {
    EO_POWER_STATUS_IDLE = 0,
    EO_POWER_STATUS_COUNT
};

void eo_nonportable_init(void);
void eo_sleep_and_EI(void);
void eo_entered_int(void);
void eo_acquire_power(enum eo_power_status status);
void eo_release_power(enum eo_power_status status);
uint64_t eo_get_now(void);
uint64_t eo_get_now_ISR(void);
void eo_set_timeout(uint64_t);
void eo_no_memory(void);

#ifdef __cplusplus
}
#endif

#endif //_EO_NON_PORTABLE_H
