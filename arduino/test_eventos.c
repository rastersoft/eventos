/*
 * Copyright 2020 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "event_os.h"
#include "serial.h"
#include <avr/boot.h>
#include <stdio.h>
#include "i2c_support.h"

int contador = 0;
int contador2 = 0;

EO_ASYNC(print_hello) {
    EO_EVENT_P msg;
    EO_BEGIN_ASYNC();
    while(1) {
        EO_EVENT_P msg = eo_new_event(EO_EVENT_TYPE_SERIAL_TX_INTERNAL_STRING);
        sprintf(msg->serial_tx_internal_string.buffer, "T: %d:%02d\n",contador/60, contador%60);
        eo_serial_send(msg);
        EO_AWAIT(-1000000);
        if ((contador % 10) == 0) {
            eo_start_calibration();
        }
        contador++;
    }
    EO_END_ASYNC();
}

EO_ASYNC(print_hello2) {
    EO_EVENT_P msg;
    EO_BEGIN_ASYNC();
    while(1) {
        EO_EVENT_P msg = eo_new_event(EO_EVENT_TYPE_SERIAL_TX_INTERNAL_STRING);
        sprintf(msg->serial_tx_internal_string.buffer, "T2: %d\n", contador2);
        eo_serial_send(msg);
        EO_AWAIT(-1500000);
        contador2++;
    }
    EO_END_ASYNC();
}

EO_ASYNC(print_hello3) {
    EO_EVENT_P msg, msg2;
    EO_BEGIN_ASYNC();
    while(1) {
        EO_AWAIT(-2000000);
        msg = eo_new_event(EO_EVENT_TYPE_I2C_INTERNAL_WRITE_READ);
        msg->i2c_internal_write_read.address = 0xD0;
        msg->i2c_internal_write_read.wr_size = 1;
        msg->i2c_internal_write_read.rd_size = 1;
        msg->i2c_internal_write_read.buffer[0] = 117;
        EO_YIELD(eo_i2c_send, msg);
        msg2 = eo_new_event(EO_EVENT_TYPE_SERIAL_TX_INTERNAL_STRING);
        if (msg->type == EO_EVENT_TYPE_I2C_OK)
            sprintf(msg2->serial_tx_internal_string.buffer, "I2C value: %X\n", msg->i2c_internal_write_read.buffer[1]);
        else
            sprintf(msg2->serial_tx_internal_string.buffer, "I2C value: ERROR\n");
        eo_destroy_event(msg);
        eo_serial_send(msg2);
    }
    EO_END_ASYNC();
}

void receive_serial(EO_EVENT_P __crust_not_null__ event) {
    int l;
    uint8_t *p;
    event->type = EO_EVENT_TYPE_SERIAL_TX_INTERNAL_STRING;
    for (l=0, p=event->serial_rx_internal_string.buffer; l < EO_EVENT_DATA_SIZE; l++, p++) {
        if (*p == 0x0A) {
            *p = 0;
            break;
        }
    }
    event->callback = NULL;
    // this is dangerous, because you must be sure that the data is correctly aligned.
    // Also, don't forget to set callback to NULL, or after sending the text,
    // eo_serial_send() will call again this same function.
    eo_serial_send(event);
}

void main2(void) {
    eo_i2c_init(100);
    eo_serial_init(0, 57600, 8, true);
    eo_start_calibration();
    eo_serial_enable_receive(receive_serial);
    EO_RUN_ASYNC_TASK(print_hello);
    EO_RUN_ASYNC_TASK(print_hello2);
    EO_RUN_ASYNC_TASK(print_hello3);
}

int main(void) {
    eo_init(main2);
    return 0;
}
