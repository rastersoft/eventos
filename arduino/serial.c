/*
 * Copyright 2020 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "serial.h"
#include <string.h>
#include <avr/power.h>

/*********************
 * PRIVATE VARIABLES *
 *********************/

static EO_EVENT_P current_tx_event = NULL;
static EO_QUEUE_T tx_event_queue;
static EO_CALLBACK_P tx_cb = NULL;
static uint8_t *tx_buffer;
static uint16_t tx_size;

static EO_CALLBACK_P rx_cb = NULL;
static EO_EVENT_P rx_event = NULL;
static uint8_t *rx_buffer;
static uint8_t rx_size;

static uint8_t usart_usage = 0;



/*********************
 * PRIVATE FUNCTIONS *
 *********************/

static void set_usart(void) {
    if (!usart_usage) {
        eo_acquire_power(EO_POWER_STATUS_IDLE);
        PRR &= 0xFD; // Enable USART
    }
    usart_usage++;
}

static void release_usart(void) {
    if (usart_usage > 0) {
        usart_usage --;
    }
    if (!usart_usage) {
        eo_release_power(EO_POWER_STATUS_IDLE);
        PRR |= 0x02; // Disable USART
    }
}

static void serial_end_function(EO_EVENT_P __crust_not_null__ event) {
    event->callback = tx_cb;
    event->type = EO_EVENT_TYPE_SERIAL_END;
    eo_launch_event(event);
    release_usart();
    eo_serial_send(eo_pop_event_head(&tx_event_queue));
}

ISR(USART_UDRE_vect) {
    eo_entered_int();
    if (UCSR0A & 0x20) {
        if (tx_size) {
            UDR0 = *tx_buffer;
            tx_buffer++;
            tx_size--;
            return;
        }
    }
    if (UCSR0A & 0x40) {
        UCSR0B &= 0x97; // disable interrupts
        current_tx_event->type = EO_EVENT_TYPE_SERIAL_END;
        eo_launch_event_ISR(current_tx_event); // notify that TX ended
        current_tx_event = NULL;
    }
}

ISR(USART_RX_vect) {
    eo_entered_int();
    uint8_t element = UDR0;
    if (!rx_event) {
        rx_event = eo_new_event_ISR(NULL, EO_EVENT_TYPE_SERIAL_RECEIVED_INTERNAL_STRING);
        rx_buffer = rx_event->serial_rx_internal_string.buffer;
        rx_size = 0;
    }
    *rx_buffer = element;
    rx_buffer++;
    rx_size++;
    if ((rx_size == EO_EVENT_DATA_SIZE) || (element == 0x0A)) {
        rx_event->callback = rx_cb;
        eo_launch_event_ISR(rx_event);
        rx_event = NULL;
    }
}

/********************
 * PUBLIC FUNCTIONS *
 ********************/

void eo_serial_init(uint8_t port, uint16_t baud, uint8_t bits, bool two_stops) {

    eo_init_queue(&tx_event_queue);
    set_usart();
    int ubrr = (1000000 / baud) - 1;
    if (bits < 5) {
        bits = 5;
    }
    if (bits > 8) {
        bits = 8;
    }
    bits -= 5;
    rx_cb = NULL;
    tx_buffer = NULL;
    tx_size = 0;

    UBRR0H = (unsigned char) (ubrr>>8);
    UBRR0L = (unsigned char) ubrr;
    UCSR0A = 0;
    UCSR0B = 0;
    UCSR0C = 6;
    if (two_stops) {
        UCSR0C |= 8;
    }
    release_usart();
}

void eo_serial_send(EO_EVENT_P __crust_not_null__ event) {

    if (!event) {
        return;
    }

    if (current_tx_event) {
        // the serial port is busy, so the event must be enqueued
        eo_push_event_tail(&tx_event_queue, event);
        return;
    }

    current_tx_event = event;
    tx_cb = current_tx_event->callback;
    if (tx_cb == eo_serial_send) {
        tx_cb = NULL;
    }
    current_tx_event->callback = serial_end_function;
    switch(current_tx_event->type) {
    case EO_EVENT_TYPE_SERIAL_TX_MINI:
        tx_buffer = current_tx_event->serial_tx_mini.buffer;
        tx_size   = current_tx_event->serial_tx_mini.size;
        break;
    case EO_EVENT_TYPE_SERIAL_TX_EXTERNAL:
        tx_buffer = current_tx_event->serial_tx_external.buffer;
        tx_size   = current_tx_event->serial_tx_external.size;
        break;
    case EO_EVENT_TYPE_SERIAL_TX_EXTERNAL_STRING:
        tx_buffer = current_tx_event->serial_tx_external_string.buffer;
        tx_size   = strlen(current_tx_event->serial_tx_external_string.buffer);
        break;
    case EO_EVENT_TYPE_SERIAL_TX_INTERNAL_STRING:
        tx_buffer = current_tx_event->serial_tx_internal_string.buffer;
        tx_size   = strlen(current_tx_event->serial_tx_internal_string.buffer);
        break;
    }
    set_usart();
    cli();
    UCSR0A |= 0x40;
    UCSR0B |= 0x68;
    sei();
}

void eo_serial_enable_receive(EO_CALLBACK_P cb) {
    DI();
    if (cb) {
        if (!rx_cb) {
            __crust_set_null__(rx_event)
            rx_buffer = NULL;
            rx_size = 0;
            set_usart();
            UCSR0A |= 0x80;
            UCSR0B |= 0x90;
        }
    } else {
        if (rx_cb) {
            UCSR0A &= 0x7F;
            UCSR0B &= 0x6F;
            release_usart();
            eo_destroy_event(rx_event);
            rx_event = NULL;
        }
    }
    rx_cb = cb;
    EI();
}
