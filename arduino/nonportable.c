/*
 * Copyright 2020 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "event_os.h"
#include <string.h>
#include <avr/sleep.h>
#include <avr/power.h>

/*********************
 * PRIVATE VARIABLES *
 *********************/

/**
 * contains the current epoch, in microseconds, since the system
 * was turned on. It must be accessed always from inside a CRITICAL section.
 */
static volatile uint64_t current_time;

/**
 * contains the period of the WD in us. It is re-evaluated each time a
 * calibration is triggered.
 */
static uint32_t calibration_value = 16384; // default value

/**
 * calibration status
 */
static enum {
    EO_CALIBRATION_IDLE,
    EO_CALIBRATION_TRIGGERED,
    EO_CALIBRATION_CONFIGURING,
    EO_CALIBRATION_WAITING,
    EO_CALIBRATION_MEASURING
} calibration_status = EO_CALIBRATION_IDLE;

/**
 * Contains the usage counter for the power statuses.
 */
static uint8_t power_status_counter[EO_POWER_STATUS_COUNT];

/**
 * Allows to know if the uC was in sleep mode when it woke up
 * with an interrupt. It is needed because, when waking up from
 * POWER_DOWN mode, the system waits 16000 cycles to ensure that
 * the quartz has stabilized, and that time must be added to the
 * timeout.
 */
static volatile bool eo_sleeping = false;

/**
 * This variable specifies if the POWER_DOWN mode is enabled or not.
 * It is faster than checking the power_status_counter
 */
static bool deep_sleep = false;


/********************
 * PUBLIC VARIABLES *
 ********************/

/**
 * contains the epoch, in microseconds, of the timeout event to be executed,
 * or UINT64_MAX if there is no pending event, and the callback to send when
 * the epoch is achieved
 */

EO_EVENT_P eo_timeout_event = NULL;

/*********************
 * PRIVATE FUNCTIONS *
 *********************/

/**
 * keeps updated the current time value with the periodic interrupts
 */
ISR(WDT_vect) {

    static EO_EVENT_P msg;
    static uint8_t old_time[2];
    static uint8_t now_time[2];

    if (calibration_status != EO_CALIBRATION_IDLE) {
        now_time[0] = TCNT1L;
        now_time[1] = TCNT1H;
    }

    switch (calibration_status) {
    case EO_CALIBRATION_TRIGGERED:
        eo_acquire_power(EO_POWER_STATUS_IDLE);
        PRR &= 0xF7; // enable Timer1
        TCCR1A = 0;
        TCCR1B = 0x03; // CTC mode, 64 prescaler
        TCNT1H = 0;
        TCNT1L = 0;
        calibration_status = EO_CALIBRATION_CONFIGURING;
        break;
    case EO_CALIBRATION_CONFIGURING:
        old_time[0] = now_time[0];
        old_time[1] = now_time[1];
        calibration_status = EO_CALIBRATION_MEASURING;
        break;
    case EO_CALIBRATION_MEASURING:
        calibration_value  = (uint32_t)now_time[1];
        calibration_value -= (uint32_t)old_time[1];
        calibration_value *= 256;
        calibration_value += (uint32_t)now_time[0];
        calibration_value -= (uint32_t)old_time[0];
        calibration_value *= 4; // each tick in the counter are 4uS

        calibration_status = EO_CALIBRATION_IDLE;
        PRR |= 0x08;
        eo_release_power(EO_POWER_STATUS_IDLE);
        break;
    default:
        break;
    }
    current_time += calibration_value;
    if (eo_sleeping && deep_sleep) {
        current_time += 1000;
    }
    if (eo_timeout_event && (eo_timeout_event->timeout.timeout <= current_time)) {
        eo_timeout_event->timeout.now = current_time;
        eo_launch_event_ISR(eo_timeout_event);
        eo_timeout_event = NULL;
    }
}

/**
 * Updates the minimum power status currently valid
 */
static void refresh_power(void) {
    deep_sleep = false;
    if (power_status_counter[0] != 0) {
        set_sleep_mode(SLEEP_MODE_IDLE);
        return;
    }
    if (power_status_counter[1] != 0) {
        set_sleep_mode(SLEEP_MODE_EXT_STANDBY);
        return;
    }
    deep_sleep = true;
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
}


/********************
 * Public functions *
 ********************/

void eo_start_calibration(void) {
    calibration_status = EO_CALIBRATION_TRIGGERED;
}

/**
 * Specifies that a driver requires a minimum power status. This function increments
 * the usage counter for the specified power status, ensuring that the system will
 * never change to a lower status than the specified. When the driver ended its work,
 * must use the call eo_release_power() to release the requirement done.
 *
 * @param status minimum power status required by the caller
 */
void eo_acquire_power(enum eo_power_status status) {
    power_status_counter[status]++;
    refresh_power();
}

/**
 * Specifies that a driver ended its requirement for a minimum power status.
 * This function decrements the usage counter for the specified power status
 *
 * @param status the power status to be freed by the caller
 */
void eo_release_power(enum eo_power_status status) {
    if (power_status_counter[status] != 0) {
        power_status_counter[status]--;
        refresh_power();
    }
}

/**
 * Initializes the non-portable section
 */

void eo_nonportable_init(void) {

    cli();
    MCUSR = 0;
    WDTCSR = 0x18;
    WDTCSR = 0xC0;
    // turn off brown-out
    uint8_t mcucr_value = MCUCR & 0x60;
    MCUCR = mcucr_value | 0x60;
    MCUCR = mcucr_value | 0x40;
    // turn off the devices not used
    PRR |= 0x6F;
    ASSR |= 0x20; // direct clock from the oscillator
    sei();

    memset(power_status_counter, 0, EO_POWER_STATUS_COUNT);
    refresh_power();
}

/**
 * Puts the CPU in low power mode.
 */
void eo_sleep_and_EI(void) {
    sleep_enable();
    sleep_bod_disable();
    sei();
    eo_sleeping = true;
    sleep_cpu();
    eo_sleeping = false;
    sleep_disable();
}

/**
 * This function must be called every time an ISR is called.
 */
void eo_entered_int(void) {
    eo_sleeping = false;
}

/**
 * Returns the current EPOCH. It must be called only from ISRs
 *
 * @return The current EPOCH since the board has been turn on, in microseconds.
 */

uint64_t eo_get_now_ISR(void) {
    return current_time;
}

/**
 * Returns the current EPOCH. It must be called only from normal code
 *
 * @return The current EPOCH since the board has been turn on, in microseconds.
 */

uint64_t eo_get_now(void) {

    uint64_t now;

    DI();
    now = current_time;
    EI();
    return now;
}

/**
 * Updates the timeout. This means that at the epoch specified, the event pointed
 * out by @eo_timeout_event must be inserted in the queue event.
 * It will be called always from normal code, never from an ISR, so it must
 * enter a critical section if needed.
 *
 * @param epoch The new epoch for the event
 */
void eo_set_timeout(uint64_t epoch) {
    DI();
    // this is the bare minimum to be done, and is valid if the timer is running
    // at the minimum period, and the epoch value is checked in each tick.
    // Add extra code if needed to trigger an interrupt after that specific
    // amount of time.
    if (eo_timeout_event) {
        eo_timeout_event->timeout.timeout = epoch;
    }
    EI();
}

/**
 * This function is called if the memory pool is exhausted. It should reset the
 * board, or freeze it, or show a message if debugging, but the execution can't
 * continue in that case.
 *
 * The only case where memory exhaustion can be detected is in ISRs
 */
void eo_no_memory(void) {
    // just freeze the system
    while(1){}
}
