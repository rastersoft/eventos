/*
 * Copyright 2020 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "i2c_support.h"
#include <stdio.h>
#include "serial.h"
#include <avr/power.h>

//* The current event being processed
static EO_EVENT_P current_event;

//* The callback of the current event
static EO_CALLBACK_P current_cb;

//* The type of the current event
static uint16_t current_type;

//* The device address
static uint8_t address;

//* Pointer to first write block
static uint8_t *first_block;
//* bytes to write from the first block
static int first_block_count;

//* Pointer to second write block
static uint8_t *second_block;
//* bytes to write from the second block
static int second_block_count;

//* Pointer to read block
static uint8_t *read_block;

//* bytes to read into the read block
static int read_block_count;

//* The queue with the pending I2C commands
static EO_QUEUE_T event_queue;


static enum _i2c_current_mode {
    I2C_MODE_SENT_START,
    I2C_MODE_SENDING_DATA,
    I2C_MODE_PREPARE_READ,
    I2C_MODE_READING_DATA,
    I2C_MODE_READING_DATA2,
    I2C_MODE_ENDED,
    I2C_MODE_WAIT_STOP,
    I2C_MODE_WAIT_STOP2,
    I2C_MODE_STOP
} mode = I2C_MODE_ENDED;

/*********************
 * PRIVATE FUNCTIONS *
 *********************/

static void i2c_end_function(EO_EVENT_P __crust_not_null__ event) {
    event->callback = current_cb;
    if ((event->type == EO_EVENT_TYPE_I2C_ERROR) && ((current_type & EO_I2C_FLAG_BITS) == EO_I2C_REPEAT_ON_ERROR)) {
        event->type = current_type;
        eo_push_event_tail(&event_queue, event); // repeat the command
    } else {
        eo_launch_event(event);
    }
    PRR |= 0x80; // Disable I2C
    eo_release_power(EO_POWER_STATUS_IDLE);
    eo_i2c_send(eo_pop_event_head(&event_queue));
}

static void send_msg(char *cadena, uint8_t v) {
    EO_EVENT_P event = eo_new_event_ISR(eo_serial_send, EO_EVENT_TYPE_SERIAL_TX_INTERNAL_STRING);
    sprintf(event->serial_tx_internal_string.buffer, cadena, v);
    eo_launch_event_ISR(event);
}

ISR(TWI_vect) {
    static uint8_t current_ack_error;
    eo_entered_int();
    __crust_set_not_null__(current_event);
    send_msg("Entro I2C\n", mode);
    switch(mode) {
    default:
        if (first_block_count || second_block_count) {
            mode = I2C_MODE_SENDING_DATA;
            TWDR = (address & 0xFE);
        } else {
            mode = I2C_MODE_READING_DATA;
            TWDR = (address | 0x01);
        }
        current_ack_error = 0x18;
        break;
    case I2C_MODE_SENDING_DATA:
        if ((TWSR & 0xF8) != current_ack_error) {
            send_msg("Error I2C %X\n",TWSR & 0xF8);
            // error
            mode = I2C_MODE_ENDED;
            TWCR = 0x95; // stop
            current_event->type = EO_EVENT_TYPE_I2C_ERROR;
            eo_launch_event_ISR(current_event);
            current_event = NULL;
            return;
        }
        current_ack_error = 0x28;
        if (first_block_count) {
            TWDR = *first_block;
            first_block_count--;
            first_block++;
            break;
        }
        if (second_block_count) {
            TWDR = *second_block;
            second_block_count--;
            second_block++;
            break;
        }
        if (read_block_count) {
            TWCR = 0xA5; // restart
            mode = I2C_MODE_PREPARE_READ;
            return;
        }
        // only write
        goto isr_ended;
    case I2C_MODE_PREPARE_READ:
        TWDR = (address | 0x01);
        mode = I2C_MODE_READING_DATA;
        break;
    case I2C_MODE_READING_DATA2:
        *read_block = TWDR;
        read_block++;
    case I2C_MODE_READING_DATA:
        read_block_count--;
        if (read_block_count) {
            TWCR = 0xC5; // ACK
            mode = I2C_MODE_READING_DATA2;
        } else {
            TWCR = 0x85; // NACK
            mode = I2C_MODE_WAIT_STOP2;
        }
        return;
    case I2C_MODE_WAIT_STOP2:
        *read_block = TWDR;
    case I2C_MODE_WAIT_STOP:
isr_ended:
        TWCR = 0x95; // STOP
        mode = I2C_MODE_ENDED;
        current_event->type = EO_EVENT_TYPE_I2C_OK;
        eo_launch_event_ISR(current_event);
        current_event = NULL;
        return;
    }
    // continue with the operation
    TWCR = 0x85;
}

/********************
 * PUBLIC FUNCTIONS *
 ********************/

/**
 * Initializates the I2C system.
 *
 * @param kbaud Speed in kbits/second. Normal values are between 100 and 400
 */

void eo_i2c_init(uint16_t kbaud) {

    eo_init_queue(&event_queue);
    eo_acquire_power(EO_POWER_STATUS_IDLE);
    PRR &= 0x7F; // Enable I2C
    mode = I2C_MODE_ENDED;
    TWSR = 0x01; // prescaler = 4 (can be 1, 4, 16, 64)
    TWBR = (uint8_t)((kbaud * 18) / 100);
    TWCR = 0x84; // enable I2C pins
    PRR |= 0x80; // Disable I2C
    DDRC = 0;
    PORTC |= 0x30;
    eo_release_power(EO_POWER_STATUS_IDLE);
}

void eo_i2c_send(EO_EVENT_P __crust_not_null__ event) {

    if (!event) {
        return;
    }

    if (current_event) {
        // the I2C bus is busy, so the event must be enqueued
        eo_push_event_tail(&event_queue, event);
        return;
    }

    current_event = event;
    current_type = current_event->type;
    current_cb = current_event->callback;
    if (current_cb == eo_i2c_send) {
        current_cb = NULL;
    }
    current_event->callback = i2c_end_function;
    second_block_count = 0;
    read_block_count = 0;

    switch(EO_I2C_FLAG_BITS & current_event->type) {
    case EO_EVENT_TYPE_I2C_INTERNAL_WRITE:
        first_block       = current_event->i2c_internal_write.buffer;
        first_block_count = current_event->i2c_internal_write.size;
        address           = current_event->i2c_internal_write.address;
        break;
    case EO_EVENT_TYPE_I2C_INTERNAL_WRITE_READ:
        first_block       = current_event->i2c_internal_write_read.buffer;
        first_block_count = current_event->i2c_internal_write_read.wr_size;
        read_block        = current_event->i2c_internal_write_read.buffer + first_block_count;
        read_block_count  = current_event->i2c_internal_write_read.rd_size;
        address           = current_event->i2c_internal_write_read.address;
        break;
    case EO_EVENT_TYPE_I2C_EXTERNAL_WRITE:
        first_block        = current_event->i2c_external_write.buffer;
        first_block_count  = current_event->i2c_external_write.internal_size;
        second_block       = current_event->i2c_external_write.external_block;
        second_block_count = current_event->i2c_external_write.external_size;
        address            = current_event->i2c_external_write.address;
        break;
    case EO_EVENT_TYPE_I2C_EXTERNAL_WRITE_READ:
        first_block       = current_event->i2c_external_write_read.buffer;
        first_block_count = current_event->i2c_external_write_read.internal_size;
        read_block        = current_event->i2c_external_write_read.external_block;
        read_block_count  = current_event->i2c_external_write_read.external_size;
        address           = current_event->i2c_external_write_read.address;
        break;
    }
    cli();
    eo_acquire_power(EO_POWER_STATUS_IDLE);
    PRR &= 0x7F; // Enable I2C
    TWCR = 0xA5; // start
    sei();
}
