#ifndef __SLEEP_AVR_REPLACEMENT_CRUST__
#define __SLEEP_AVR_REPLACEMENT_CRUST__

#include <stdint.h>
#include <stdbool.h>

extern uint8_t TCNT1H;
extern uint8_t TCNT1L;
extern uint8_t TCCR1A;
extern uint8_t TCCR1B;

#define SLEEP_MODE_IDLE 0
#define SLEEP_MODE_EXT_STANDBY 1
#define SLEEP_MODE_PWR_DOWN 2

void set_sleep_mode(int);
void sleep_enable(void);
void sleep_bod_disable(void);
void sleep_cpu(void);
void sleep_disable(void);

#endif
