#ifndef __INTERRUPT_AVR_REPLACEMENT_CRUST__
#define __INTERRUPT_AVR_REPLACEMENT_CRUST__

#define ISR(NAME) void NAME_int(void)

void cli(void);
void sei(void);

#endif
