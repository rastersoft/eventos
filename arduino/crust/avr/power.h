#ifndef __POWER_AVR_REPLACEMENT_CRUST__
#define __POWER_AVR_REPLACEMENT_CRUST__

#include <stdint.h>
#include <stdbool.h>

extern uint8_t PRR;
extern uint8_t ASSR;
extern uint8_t MCUSR;
extern uint8_t MCUCR;
extern uint8_t WDTCSR;

extern uint8_t UCSR0A;
extern uint8_t UCSR0B;
extern uint8_t UCSR0C;
extern uint8_t UDR0;
extern uint8_t UBRR0H;
extern uint8_t UBRR0L;
extern uint8_t RXEN0;
extern uint8_t RXCIE0;


extern uint8_t TWDR;
extern uint8_t TWCR;
extern uint8_t TWBR;
extern uint8_t TWSR;

extern uint8_t DDRC;

extern uint8_t PORTC;
#endif
