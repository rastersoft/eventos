/*
 * Copyright 2020 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _EO_SERIAL_H_
#define _EO_SERIAL_H_

#include "event_os.h"

#ifdef __cplusplus
extern "C" {  // only need to export C interface if
              // used by C++ source code
#endif

// valid event types for a serial TX request

#define EO_EVENT_TYPE_SERIAL_TX_MINI 0
#define EO_EVENT_TYPE_SERIAL_TX_EXTERNAL 1
#define EO_EVENT_TYPE_SERIAL_TX_INTERNAL_STRING 2
#define EO_EVENT_TYPE_SERIAL_TX_EXTERNAL_STRING 3

void eo_serial_init(uint8_t port, uint16_t baud, uint8_t bits, bool two_stops);
void eo_serial_send(EO_EVENT_P __crust_not_null__ event);
void eo_serial_enable_receive(EO_CALLBACK_P);

#ifdef __cplusplus
}
#endif

#endif // _EO_SERIAL_H_
