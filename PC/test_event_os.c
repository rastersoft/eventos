/*
 * Copyright 2020 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "event_os.h"
#include <stdio.h>

int contador = 0;
int contador2 = 0;

EO_ASYNC(print_hello) {
    EO_BEGIN_ASYNC();
    while(1) {
        printf("T: %d:%02d\n",contador/60, contador%60);
        EO_AWAIT(1000000);
        contador++;
    }
    EO_END_ASYNC();
}

EO_ASYNC(print_hello2) {
    EO_BEGIN_ASYNC();
    while(1) {
        printf("T2: %d\n", contador2);
        EO_AWAIT(1500000);
        contador2++;
        if (contador2 == 5) {
            EO_EXIT_ASYNC();
        }
    }
    EO_END_ASYNC();
    printf("Exiting hello2\n");
}

void main2(void) {
    EO_RUN_ASYNC_TASK(print_hello);
    EO_RUN_ASYNC_TASK(print_hello2);
}

int main(void) {
    eo_init(main2);
    return 0;
}
