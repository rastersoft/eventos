/*
 * Copyright 2020 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "event_os.h"
#include <sys/time.h>
#include <signal.h>

/*********************
 * PRIVATE VARIABLES *
 *********************/

#define TIME_INTERVAL 50000

/**
 * contains the current epoch, in microseconds, since the system
 * was turned on. It must be accessed always from inside a CRITICAL section.
 */
static volatile uint64_t current_time;

/**
 * contains the epoch, in microseconds, of the timeout event to be executed,
 * or UINT64_MAX if there is no pending event, and the callback to send when
 * the epoch is achieved
 */

static EO_EVENT_P timeout_event = NULL;

/********************
 * PUBLIC VARIABLES *
 ********************/

/**
 * contains the epoch, in microseconds, of the timeout event to be executed,
 * or UINT64_MAX if there is no pending event, and the callback to send when
 * the epoch is achieved
 */

EO_EVENT_P eo_timeout_event = NULL;

/*********************
 * PRIVATE FUNCTIONS *
 *********************/

/**
 * keeps updated the current time value with the periodic interrupts
 */
void alrmhandle(int v) {

    static EO_EVENT_P msg;
    current_time += TIME_INTERVAL;
    if (eo_timeout_event && (eo_timeout_event->timeout.timeout <= current_time)) {
        eo_timeout_event->timeout.now = current_time;
        eo_launch_event_ISR(eo_timeout_event);
        eo_timeout_event = NULL;
    }
}


/********************
 * Public functions *
 ********************/

/**
 * Specifies that a driver requires a minimum power status. This function increments
 * the usage counter for the specified power status, ensuring that the system will
 * never change to a lower status than the specified. When the driver ended its work,
 * must use the call eo_release_power() to release the requirement done.
 *
 * @param status minimum power status required by the caller
 */
void eo_acquire_power(enum eo_power_status status) {
    // do nothing
}

/**
 * Specifies that a driver ended its requirement for a minimum power status.
 * This function decrements the usage counter for the specified power status
 *
 * @param status the power status to be freed by the caller
 */
void eo_release_power(enum eo_power_status status) {
    // do nothing
}

/**
 * Initializes the non-portable section
 */
void eo_nonportable_init(void) {

    struct itimerval it;
    struct sigaction sa;
    static struct _eo_event timeout_event_original;

    timeout_event = &timeout_event_original;

    sa.sa_handler = alrmhandle;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGALRM , &sa, NULL);

    it.it_interval.tv_sec = 0;
    it.it_interval.tv_usec = TIME_INTERVAL;
    it.it_value.tv_sec = 0;
    it.it_value.tv_usec = TIME_INTERVAL;
    setitimer(ITIMER_REAL, &it, NULL);
}

void eo_sleep_and_EI(void) {
    // do nothing
}

/**
 * This function must be called every time an ISR is called.
 */
void eo_entered_int(void) {
    // do nothing
}

/**
 * Returns the current EPOCH. It must be called only from ISRs
 *
 * @return The current EPOCH since the board has been turn on, in microseconds.
 */

uint64_t eo_get_now_ISR(void) {
    return current_time;
}

/**
 * Returns the current EPOCH. It must be called only from normal code
 *
 * @return The current EPOCH since the board has been turn on, in microseconds.
 */

uint64_t eo_get_now(void) {

    uint64_t now;

    DI();
    now = current_time;
    EI();
    return now;
}

/**
 * Updates the timeout. This means that at the epoch specified, the event pointed
 * out by @eo_timeout_event must be inserted in the queue event.
 * It will be called always from normal code, never from an ISR, so it must
 * enter a critical section if needed.
 *
 * @param epoch The new epoch for the event
 */
void eo_set_timeout(uint64_t epoch) {
    DI();
    // this is the bare minimum to be done, and is valid if the timer is running
    // at the minimum period, and the epoch value is checked in each tick.
    // Add extra code if needed to trigger an interrupt after that specific
    // amount of time.
    if (eo_timeout_event) {
        eo_timeout_event->timeout.timeout = epoch;
    }
    EI();
}

/**
 * This function is called if the memory pool is exhausted. It should reset the
 * board, or freeze it, or show a message if debugging, but the execution can't
 * continue in that case.
 *
 * The only case where memory exhaustion can be detected is in ISRs
 */
void eo_no_memory(void) {
    // just freeze the system
    while(1){}
}
