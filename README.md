# EventOS

## What is it?

EventOS (or event_os) is a very simple nano operating system for microcontrollers. It is built around an event/message
queue and borrows a lot of ideas from microkernels, but without a true microkernel, since there are not true
tasks/processes and the multitasking is cooperative. These characteristics makes it perfect for microcontrollers,
because it is translated into a very low memory and resources consumption. It offers several advantages over bare-metal
coding:

  * lightweight threads: you can have several "tasks" running in parallel without effort
  * easy interrupt management: just do the basic task in the ISR and send an event/message with the result, which
  will be processed in its own task.
  * implicit power management: you can just code, and EventOS will decide automagically when to enter/exit low power
  modes

The tasks are based on [protothreads](http://dunkels.com/adam/pt/), and are integrated in the main loop and the
event/message queue, so they work transparently and are very easy to work with.

## Internal description

The core of EventOS is an event/message queue, the functions to add events/messages into it from both "user space" code
and from ISRs (of course there is no "user space" as it, but non-ISR code), a main loop, and a definition of "drivers".

The main repository has four main files:

  * event_os.c and event_os.h: contains the common, portable code, and the headers.
  * nonportable.c and nonportable.h: contains the code that must be rewritten and adapted for each microcontroller.

Also, currently there are two extra folders:

  * PC: contains a very basic port to Posix. It has no device drivers, but can execute basic code with things like
  launching tasks and waiting some timeout.
  * Arduino: a port to Arduino UNO board (in fact, it is more a port to ATMega 328P microcontroller because it doesn't
  use arduino specific code). It includes serial port and I2C drivers.

The arduino port uses the watchdog as timer to allow for maximum power saving, and to improve the precision it includes
an autocalibration function (eo_start_calibration(), only in arduino) to detect the true period. This function uses the
timer 2 during a brief period of time to compare it with the time passed between watchdog ticks, and then turns it off.
It is possible to call it periodically (a good period is once every two-five minutes) to recalibrate the timer and take
into account variations due to changes in temperature or voltage. Of course, in a board with an external 32768Hz quartz,
a more precise and lower power consumption can be implemented.

All the code is annotated with CRUST tokens. These allows to pass the CRUST static analyzer and detect in advance memory
leaks and dangling pointers, but it is still pure C language. For more details, check https://gitlab.com/rastersoft/crust

There are four typedefs:

  * **EO_EVENT** is an structure that contains an event/message. It is used to pass the parameters to any function, and to
  receive the results.
  * **EO_EVENT_P** is a pointer to an EO_EVENT structure.
  * **EO_CALLBACK_P** is a pointer to a callback function. It has **void function(EO_EVENT_P);** signature, and is the
  base of the system.
  * **EO_QUEUE_T** is a structure that contains a queue. This is used in several parts, and that's why it is made generic.
  It can store **EO_EVENT** blocks, and can work as a FIFO or LIFO queue.

## The EO_EVENT structure (and EO_EVENT_P pointer to it)

As explained before, the system is built around the EO_EVENT structure, and it is used to pass all the data needed between
functions and tasks. This is useful because a call can be easily stored if the destination task is busy (an example is the
serial driver: if a task wants to send a text through the serial port, but it is busy sending a previous message, the
EO_EVENT structure will be stored in a queue and sent as soon as the serial port becomes free).

It has five elements used internally by EventOS. These are:

  * **next**, used internally to store the event/message in queues. Must not be used by the programmer.
  * **callback** is of type EO_CALLBACK_P. When an event/message is passed directly to a task/function, it contains the
  return function. When that task/function ends its job, it will store the results in that same event/message and insert
  it in the mainloop queue. This way, when the event/message arrives to the top of that queue, it will be sent to the
  corresponding function. It must be set by the programmer, unless it uses the **YIELD** macro (more on this later).
  * **entry_point** is used by **YIELD** and **AWAIT** to know where to return inside an async function (more on this later).
  * **data** is used to store temporary data from a **YIELD** and **AWAIT** macro.
  * **type** is a semi-free value, used to identify the type of the event/message (and thus how to interpret the rest of the data in
  the structure). The values from 0xC000 up are reserved for EventOs, to identify answers from drivers and other standard
  elements. Any other value can be used when calling an asynchronous function or task, and even can be repeated, because
  they are specific for each asynchronous function (for example, type 2 is "write data from an external buffer" for I2C,
  but "write zero-ended string from an external buffer" for serial port).

Generally speaking, the programmer only has to fill the **type** element, and optionally the **callback** one when doing
a call directly instead of using **YIELD** or **AWAIT**.

After these elements come an union of several structures, each one with its own name. By convention, its name is the
same than the #define use to define the event type for an event/message that uses it. So, if we check the available
structures, we can find, between others, **serial_tx_mini**, **serial_tx_internal_string**, **i2c_internal_write_read**
and more, and, by convention, we will use **EO_EVENT_TYPE_SERIAL_TX_MINI**, **EO_EVENT_TYPE_SERIAL_TX_EXTERNAL_STRING**
and **EO_EVENT_TYPE_I2C_INTERNAL_WRITE_READ** respectively in the **type** element. Thus, if we called the I2C driver
with an event/message of type **EO_EVENT_TYPE_I2C_INTERNAL_WRITE_READ**, we know that the data for that call must be
stored at **event->i2c_internal_write_read**.

## Initialization

The first call to do is **eo_init(EO_CALLBACK_P main2)**. This function initializes the base system and the event queue,
calls the **main2()** function (passed as a pointer) and enters the main loop. It must be called in **main()**.

Once inside **main2()** we can initialize other elements, like the drivers, and launch tasks that will run in parallel.

## Memory management

As explained before, all the system is built around EO_EVENT_P, which is a pointer to an structure that defines a
message. The structure itself, EO_EVENT, can be used to statically reserve memory for a message/event, but it is
important to keep in mind that, in that case, CRUST won't be as useful. Only if you know well what you are doing is
a good idea to use it.

EventOS has a dynamic memory allocator, which reserve a piece of memory and divide it in EO_EVENT structures. Since all
have the same size, there is no external fragmentation, and allows to get a memory block in constant time, because they
are stored in a LIFO queue. This allows to use it even inside ISRs. To define the size of the memory block, just adjust
the value of **FAST_HEAP_SIZE** in the file **event_os.h**.

To obtain a new, fresh event block, just use **eo_new_event()**, or **eo_new_event_ISR()** if doing it from an ISR. It
will return a pointer to a new block. If the pool is exhausted and is called from an ISR, it will return NULL; but if
it was called from normal code, the function **eo_no_memory()** will be called, which should reset the system, freeze
it, or show a debug message. This is because dynamic memory is used in the asynchronous calls **AWAIT** and **YIELD**
(explained later), and it isn't possible to do a graceful management in that case.

**eo_new_event()** guarantees that all the block has been zeroed, but **eo_new_event_ISR()** only zeroes **data**,
and **entry_point**.

When an event/message is no longer needed, it can be returned to the pool with **eo_destroy_event()**.

To avoid memory leaks and dangling pointers, all the code is annotated using **CRUST**. It is an static analyzer for C
that allows to implement some of the same safeguards and memory management that has the **RUST** language, without
any kind of runtime and keeping the code as pure C.

There are also two other calls, **eo_request_memory_block()** and **eo_free_memory_block** that also returns and recycles
a block of memory of the same size than **EO_EVENT**, and in fact they are stored in the same heap used by the previous
calls. The advantage is that it uses a **void** pointer, so those allocations and frees are ignored by **CRUST**, so
they are useful for non-safe pieces of code.

## The main loop

After executing all the initialization code in the function specified in **eo_init()**, EventOS will enter the main loop.
Here, it will check if there are new events/messages waiting in the queue, in which case it will pop them one by one and call
the function passed in the **callback** element of the event/message, and passing that same event/message as parameter.
It is important to remember that it is passed as-is, which means that the **callback** parameter still contains the function
being called. That means that care must be taken when recycling the received event, because sending it again to the event
queue without changing or setting the **callback** pointer can result in an infinite loop.

## Power management

If there aren't events/messages pending, the main loop will set the processor in low-power state until an interrupt
happens. This allows to build low power systems easily. But since a task can be waiting for an interrupt generated by
a device that need a not-so-low-power state, there are two extra functions, designed to be used from drivers:
**eo_acquire_power(POWER_STATE)** and **eo_release_power(POWER_STATE)**. Whenever an external device that requires
the processor to be put at most at an specific power state, the driver code must execute **eo_acquire_power(POWER_STATE)**
specifying that minimum power state; and when it is done and the device is disabled, it must call
**eo_release_power(POWER_STATE)** with that same power state. The code will keep track of all the calls done, and when
the main loop decides that it is time to sleep, will go only to the minimum currently allowed power state.

Here is an example with the ATMega 328P. This microcontroller has several modes: Idle, ADC Noise Reduction, Power-down,
Power-save, Standby and Extended Standby. But since we have drivers for only four external devices (USART, I2C and Watchdog),
we only need to reserve one power status: Idle. When we aren't waiting for any of the devices to complete, we can safely
go to Power-down mode and let the Watchdog to wake up whenever we need. But if we send something to the serial port, a
call to **eo_acquire_power(EO_IDLE)** must be done to ensure that the system will remain in Idle mode when there are no
events waiting, and thus allow the USART to work. The same happens with the I2C device, which also requires a call to
**eo_acquire_power(EO_IDLE)**. The system counts how many times a power mode has been requested, thus exactly the same
number of **eo_release_power()** calls must be done with the same power state than **eo_acquire_power()** calls. This
ensures that everything works fine even if a string is send to the serial port at the same time than data is being read
or written to an I2C device.

Of course, if in the future other devices are implemented, new power status modes can be implemented to take more advantage
of them, but currently only Idle and Power-down are needed in the ATMega.

## Tasks and asynchronous functions

As commented previously, EventOS includes support for asynchronous functions and lightweight tasks integrated in the
event queue. This greatly simplifies the code.

To launch a new task we can use **EO_RUN_ASYNC_TASK(EO_CALLBACK_P task_name)** or
**EO_RUN_ASYNC_TASK_P(EO_CALLBACK_P task_name, EO_EVENT_P parameters)** if we want to pass an event structure with
initialization data.

A task is an asynchronous function, which is defined with a series of macros. An example is:

```c
EO_ASYNC(async_demo) {

    EO_EVENT_P msg; // define here variables. Be careful with static ones if you use reentrance

    EO_BEGIN_ASYNC();
    // do things
    EO_END_ASYNC();
}
```

This piece of code is expanded into this piece of code. It is shown only for illustration purposes, it isn't really
needed to know this:

```c
void async_demo(EO_EVENT_P data) {
    static EO_CALLBACK_P _current_function_p_ = async_demo;

    EO_EVENT_P msg; // define here variables. Be careful with static ones if you use reentrance

    switch((data && (data->callback == _current_function_p_)) ? data->retcode : 0) {
    default:
        // do things
        // ...

        // return the result of the computation inside the event
        eo_launch_event(data);
    }
}
```
Here we can see that we receive all the parameters in the **data** event/message. Also, the result must be returned in that
same structure, where the return callback and other data needed is already stored. This is automagically done with
**eo_launch_event(data)**, which inserts the event/message in the message queue, ready to be processed after any previous
message, and again, since that call is inserted by the macros, the programmer doesn't need to think about that.

There are two macros that can be used inside tasks/asynchronous functions to yield the control to another asynchronous function:

  * **EO_YIELD(function, message)**: it will call **function** passing **message** as its parameter. Internally it will automagically
  store the current function as the return callback in **message** and the entry point in **entry_point**, and will also store
  the current **data** message in the **data** parameter of **message**, to be able to recover it when returning from **function**.
  But all this is done internally and you don't need to care about it: after the call has ended, everything will be restored
  as expected, and the result of the asynchronous function will be in the same **message** structure that you used to call it.
  Of course, the local variables can't be stored, so it is responsibility of the programmer to store them in the **data**
  message if possible, or in static variables inside the function (but in that case, it won't be possible to re-enter the
  same asynchronous function). To understand better why this is this way, review the *protothreads* concept: http://dunkels.com/adam/pt/

  * **EO_AWAIT(timeout)**: it will suspend the current task during **timeout** microseconds, passing the execution to the next
  message waiting in the message queue. Again, internally it will create a new message, store the **data** message inside it,
  and restore everything on return, but, also again, the programmer doesn't need to know how it does work. If **timeout**
  is a positive value, it will wait that amount of time (approximately, because this isn't a real time OS), but if it
  is a negative value, it will wait until the end of the current **timeout** period. This is: if the value is -1000000
  (this is, one second), and the current epoch is 13040250, it will wait until epoch 14000000. If the value is -2500000,
  and the current epoch is 11052689, it will wait until epoch 1250000 (2500000 * 5). This is useful to guarantee that
  periodic tasks have good precision, because the code inside needs some time to execute, time that must be subtracted
  from the desired period. Also, this synchronizes the tasks, waking up them at the same time and reducing the number of
  changes from idle to running, which reduces the power consumption.

It is important to remember that, internally, the implementation of protothreads uses a hidden *switch/case* structure,
which means that it is probable that you won't be able to use *swith/case* inside a task or asynchronous function (unless
your compiler supports nested *switch/case* structures, of course).

Here is a practical example of asynchronous tasks: it sends the value 117 to the device 0x68 over the I2C bus, reads
one byte, prints the result using the serial port, and repeats this forever every two seconds.

```c
EO_ASYNC(i2c_example) {
    EO_EVENT_P msg, msg2;
    EO_BEGIN_ASYNC();
    while(1) {
        // wait two seconds
        EO_AWAIT(-2000000);

        // I2C petition
        msg = eo_new_event(EO_EVENT_TYPE_I2C_INTERNAL_WRITE_READ);
        msg->i2c_internal_write_read.address = 0xD0; // the address must be rotated one bit to the left. It is 0x68
        msg->i2c_internal_write_read.wr_size = 1; // write one byte
        msg->i2c_internal_write_read.rd_size = 1; // read one byte
        msg->i2c_internal_write_read.buffer[0] = 117; // the byte to write is 117
        EO_YIELD(eo_i2c_send, msg); // wait until the operation is done (will switch to other tasks meanwhile)

        // Print the result
        msg2 = eo_new_event(EO_EVENT_TYPE_SERIAL_TX_INTERNAL_STRING);
        if (msg->type == EO_EVENT_TYPE_I2C_OK)
            sprintf(msg2->serial_tx_internal_string.buffer, "I2C value: %X\n", msg->i2c_internal_write_read.buffer[1]);
        else
            sprintf(msg2->serial_tx_internal_string.buffer, "I2C value: ERROR\n");
        eo_destroy_event(msg);
        eo_serial_send(msg2);
    }
    EO_END_ASYNC();
}
```



## Exiting from an asynchronous function

It is important to insert in the message queue the event with the result of an asynchronous function to ensure that any
calling asynchronous function can continue its execution. By default this is done automagically when the function reaches
the **EO_END_ASYNC();** statement. But if you want to exit in the middle of the function, you can't just call **return;** because
the **data** message won't be inserted in the message queue.

Thus in that case you have two options:

  * you can call **eo_launch_event(data);** and then **return;**, or
  * you can use **EO_EXIT_ASYNC();**, which will insert the message in the message queue as needed and return (this is, does
  exactly what the previous line says, but in a single statement), or
  * you can add a label before **EO_END_ASYNC();** and jump into it with **goto ...;**. This will also execute any code located
  after **EO_END_ASYNC();**.

## Available drivers

Currently there are two drivers defined: UART, and I2C, both only in the ATMega microcontroller.

### UART driver

The UART driver is initialized with the function:

```c
void eo_serial_init(uint8_t port, uint16_t baud, uint8_t bits, bool two_stops)
```

The **port** parameter specifies which serial port is being initialized (the ATMega has only one, but other future uC
can have more). **baud** sets the speed, **bits** the number of bits per word (5, 6, 7 or 8), and the las, **two_stops**,
wether there will be sent one or two stop bits. This call must be called after **eo_init()**.

After that, it is possible to send characters through the serial port by calling:

```c
void eo_serial_send(EO_EVENT_P __crust_not_null__ event)
```

This function receives a pointer to an event/message with any of these types:

  * EO_EVENT_TYPE_SERIAL_TX_MINI
  * EO_EVENT_TYPE_SERIAL_TX_EXTERNAL
  * EO_EVENT_TYPE_SERIAL_TX_INTERNAL_STRING
  * EO_EVENT_TYPE_SERIAL_TX_EXTERNAL_STRING

The parameters for each one are in the corresponding structure in the last union. The MINI and EXTERNAL types are for
sending binary data. The former allows up to 21 bytes of data, stored in the event/message structure itself, and the
later just contains a pointer to the data. In both cases the **size** element contains how many bytes must be sent.

The INTERNAL_STRING and EXTERNAL_STRING are for sending zero-terminated strings. The former allows to send strings of
up to 22 bytes (including the trailing zero) stored inside the event/message structure, and the later just contains
a pointer to an external memory block with the string.

The send operation is done by interrupts, and if the serial port is in use, still transmitting a previous string, the
event/message will be stored in a queue owned by the driver. In both cases the control returns immediately to the caller,
which can continue doing other tasks or return to the main loop.

When the string in an event/message has been sent, the event type of that memory block is changed to EO_EVENT_TYPE_SERIAL_END,
and the event/message is inserted in the mainloop queue. Thus, if **eo_serial_send()** was called with **YIELD**, the 
execution will continue with the next instruction. But if instead of using **YIELD**, the caller just called directly to
**eo_serial_send()**, if a function pointer was set in the callback element, it will be called after passing through the
mainloop queue; but if it was set to NULL, the event/message will be returned to the heap.

(To be continued)

# Contacting the author

Sergio Costas  
rastersoft@gmail.com  
https://www.rastersoft.com  
https://gitlab.com/rastersoft
