/*
 * Copyright 2020 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*****************************************
 * This file contains the non-portable   *
 * base part for eventOS. But other non  *
 * portable parts will be the drivers    *
 * for internal devices, like I2c, which *
 * won't be added here, but in separate  *
 * files.                                *
 *****************************************/

#include "event_os.h"

/*********************
 * PRIVATE VARIABLES *
 *********************/


/********************
 * PUBLIC VARIABLES *
 ********************/

/**
 * Contains a timeout event that will be added to the event queue when the
 * current epoch is equal or greater than the *timeout* parameter in the
 * *timeout* structure (this is: eo_timeout_event->timeout->timeout).
 * The initialization function must configure some kind of timer.
 *
 * It is important to take into account that this value can be NULL.
 */

EO_EVENT_P eo_timeout_event = NULL;

/*********************
 * PRIVATE FUNCTIONS *
 *********************/


/********************
 * PUBLIC FUNCTIONS *
 ********************/

/**
 * Specifies that a driver requires a minimum power status. This function increments
 * the usage counter for the specified power status, ensuring that the system will
 * never change to a lower status than the specified. When the driver ended its work,
 * must use the call eo_release_power() to release the requirement done.
 *
 * This function and its antagonist are used with the internal devices.
 *
 * The available power modes are defined in nonportable.h.
 *
 * @param status minimum power status required by the caller
 */
void eo_acquire_power(enum eo_power_status status) {

}

/**
 * Specifies that a driver ended its requirement for a minimum power status.
 * This function decrements the usage counter for the specified power status
 *
 * @param status the power status to be freed by the caller
 */
void eo_release_power(enum eo_power_status status) {

}

/**
 * Initializes the non-portable section, and prepares the timer for the
 * timeout.
 */
void eo_nonportable_init(void) {


}

/**
 * When this function is called, the processor must enter into low-power state,
 * and being able to be woke up by an interrupt/semaphore/whichever system is
 * used to manage exclusion zones. It also must enter up to the maximum power
 * status allowed by the previous calls to eo_acquire_power() and
 * eo_release_power().
 *
 * This call will be called after having called DI(), that's why it must do
 * the EI part before returning.
 */
void eo_sleep_and_EI(void) {

}

/**
 * This function must be called every time an ISR is called, at the beginning.
 * It is needed for arduino.
 */
void eo_entered_int(void) {
}

/**
 * Returns the current EPOCH. It must be called only from ISRs
 *
 * @return The current EPOCH since the board has been turn on, in microseconds.
 */

uint64_t eo_get_now_ISR(void) {

}

/**
 * Returns the current EPOCH. It must be called only from normal code, so it
 * can disable interruptions if needed.
 *
 * @return The current EPOCH since the board has been turn on, in microseconds.
 */

uint64_t eo_get_now(void) {

}

/**
 * Updates the timeout. This means that at the epoch specified, the event pointed
 * out by @eo_timeout_event must be inserted in the queue event.
 * It will be called always from normal code, never from an ISR, so it must
 * enter a critical section if needed.
 *
 * @param epoch The new epoch for the event
 */
void eo_set_timeout(uint64_t epoch) {
    DI();
    // this is the bare minimum to be done, and is valid if the timer is running
    // at the minimum period, and the epoch value is checked in each tick.
    // Add extra code if needed to trigger an interrupt after that specific
    // amount of time.
    if (eo_timeout_event) {
        eo_timeout_event->timeout.timeout = epoch;
    }
    EI();
}

/**
 * This function is called if the memory pool is exhausted. It should reset the
 * board, or freeze it, or show a message if debugging, but the execution can't
 * continue in that case.
 *
 * The only case where memory exhaustion can be detected is in ISRs
 */
void eo_no_memory(void) {
    // just freeze the system
    while(1){}
}
