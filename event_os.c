/*
 * Copyright 2020 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "event_os.h"
#include "nonportable.h"

#include <stdlib.h>
#include <string.h>


/*********************
 * PRIVATE VARIABLES *
 *********************/

/**
 * this structure contains the events heap, from where the memory
 * blocks are taken.
 *
 * It must be accessed always from inside a CRITICAL section
 */
static EO_QUEUE_T fast_heap;

/**
 * this structure contains the main event queue
 *
 * It must be accessed always from inside a CRITICAL section.
 */
static EO_QUEUE_T mainloop;

/**
 * This queue contains the timeouts waiting to be serverd
 */
static EO_QUEUE_T timeout_queue;

/*
 * This block is from where the events heap is generated.
 * Do not use.
 */
static struct _eo_event heap_events[FAST_HEAP_SIZE];

/*********************
 * PRIVATE FUNCTIONS *
 *********************/

/**
 * Launches the main eventloop
 */

static void eo_run(void) {
    EO_EVENT_P event;
    EO_CALLBACK_P cb;

    while(1) {
        DI();
        event = eo_pop_event_head(&mainloop);
        if (!event) {
            eo_sleep_and_EI();
            continue;
        }
        EI();
        cb = event->callback;
        cb(event); __crust_no_warning__
    }
}

/**
 * Creates an event ready to be used in a relative timeout
 *
 * @param timeout microseconds to wait. If it is negative, it will use ROUND_TO_PERIOD
 * @param data a pointer to an event to store and recover after the timeout
 * @return an event, ready to be used with YIELD
 */
EO_EVENT_P __crust_not_null__ eo_build_timeout(int64_t timeout, EO_EVENT_P data) {
    EO_EVENT_P event = eo_new_event(EO_EVENT_TYPE_TIMEOUT);
    if (timeout >= 0) {
        event->timeout.timeout = timeout;
        event->timeout.flags = EO_TIMEOUT_RELATIVE;
    } else {
        event->timeout.timeout = -timeout;
        event->timeout.flags = EO_TIMEOUT_ROUND_TO_PERIOD;
    }
    event->data = data;
    return event;
}

/**
 * Restores the event after a YIELD, and destroys the original.
 *
 * @param event The event received in the callback
 * @return The data event
 */

EO_EVENT_P eo_recover_data(EO_EVENT_P event) {
    if (event) {
        EO_EVENT_P data = event->data;
        eo_destroy_event(event);
        return data;
    } else {
        return NULL;
    }
}

/**
 * Manages the timeout function. It is called when the last timeout set expired. It sends
 * all the pending events for that timeout and prepares the next timeout.
 *
 * @param event The timeout event
 */
void eo_timeout_manage(EO_EVENT_P __crust_not_null__ event) {
    __crust_set_null__(eo_timeout_event); // it was set to NULL by the ISR
    uint64_t now = event->timeout.now;
    event->timeout.timeout = UINT64_MAX;
    event->callback = eo_timeout_manage;
    eo_timeout_event = event;
    while ((timeout_queue.head) && (timeout_queue.head->timeout.timeout <= now)) {
        event = eo_pop_event_head(&timeout_queue);
        event->type = EO_EVENT_TYPE_TIMEOUT; __crust_no_warning__;
        eo_launch_event(event);
    }

    if (timeout_queue.head) {
        eo_set_timeout(timeout_queue.head->timeout.timeout);
    }
}

/********************
 * PUBLIC FUNCTIONS *
 ********************/

/**
 * Initializes a queue
 */

void eo_init_queue(EO_QUEUE_T *queue) {
    if (queue == NULL) {
        return;
    }
    queue->head = NULL;
    queue->tail = NULL;
}

/**
 * Initializes everything: static variables, the main timer...
 * This function doesn't return, because it enters the main loop.
 *
 * @param cb         A pointer to a function called after initialization has been done
 */
void eo_init(__crust_not_null__ void(*cb)(void)) {

    __crust_set_null__(eo_timeout_event);
    // this memory block will be used for the timeout events
    static EO_EVENT timeout_event_original;

    timeout_event_original.callback = eo_timeout_manage;
    timeout_event_original.timeout.timeout = UINT64_MAX;
    // this trick allows to reuse the same event over and over again, thus avoiding
    // the use of dynamic memory in this case.
    eo_timeout_event = (EO_EVENT_P)&timeout_event_original;

    // Fill the events heap
    eo_init_queue(&fast_heap);
    eo_init_queue(&mainloop);
    eo_init_queue(&timeout_queue);
__crust_disable__
    for(int loop=0;loop < FAST_HEAP_SIZE; loop++) {
        eo_push_event_head(&fast_heap, heap_events + loop);
    }
__crust_enable__

    eo_nonportable_init();
    cb();
    eo_run();
}

/**
 * Inserts an event in an specific queue at the head
 * @param queue A pointer to the queue into which to insert the event
 * @param event A pointer to the event to insert in the queue
 */
void eo_push_event_head(EO_QUEUE_T *queue, EO_EVENT_P event) {
    if (event == NULL) {
        return;
    }
    event->next = queue->head;
    queue->head = event;
    if (event->next == NULL) {
        queue->tail = queue->head;
    }
}

/**
 * Inserts an event in an specific queue at the tail
 * @param queue A pointer to the queue into which to insert the event
 * @param event A pointer to the event to insert in the queue
 */
void eo_push_event_tail(EO_QUEUE_T *queue, EO_EVENT_P event) {
    if (event == NULL) {
        return;
    }
    event->next = NULL;
    if (queue->head == NULL) {
        queue->head = event;
        queue->tail = queue->head;
        return;
    }
    queue->tail->next = event;
    queue->tail = queue->tail->next;
}

/**
 * Returns an event from the head of the specified queue, removing it.
 * @param queue A pointer to the queue from which remove the event
 * @return      A pointer to the event that was in the head of the queue
 */
EO_EVENT_P eo_pop_event_head(EO_QUEUE_T *queue) {
    if (queue->head == NULL) {
        return NULL;
    }
    EO_EVENT_P event = queue->head;
    queue->head = event->next;
    return event;
}

/**
 * Returns the event at the head of the specific queue without removing it.
 * @param queue A pointer to the queue from which remove the event
 * @return      A pointer to the event at the head of the queue
 */
EO_EVENT_P eo_peek_event_head(EO_QUEUE_T *queue) {
    return (EO_EVENT_P) queue->head;
}

/**
 * Pushes an event in a queue ordered by the timeout->timeout value
 * @param queue A pointer to the queue into which insert the event
 * @param event The event to insert into the queue
 */
void eo_push_event_sorted(EO_QUEUE_T *queue, EO_EVENT_P __crust_not_null__ event) {

    // add the event ordered
    if ((queue->head == NULL) || (queue->head->timeout.timeout > event->timeout.timeout)) {
        eo_push_event_head(queue, event);
        return;
    }
    for(EO_EVENT_P __crust_alias__ qevent = queue->head; qevent != NULL; qevent = qevent->next) {
        if ((qevent->next != NULL) && (qevent->next->timeout.timeout > event->timeout.timeout)) {
            event->next = qevent->next;
            qevent->next = event;
            return;
        }
    }
    eo_push_event_tail(queue, event);
}

/**
 * Inserts all the elements of the ORIGIN queue at the tail of
 * DESTINATION queue, also removing them from the original queue.
 *
 * @param destination The queue into which the events will be inserted
 * @param origin The queue from which the events will be taken
 */
void eo_mix_queues(EO_QUEUE_T *destination, EO_QUEUE_T *origin) {
    if (!origin->head) {
        return;
    }

    if (destination->head == NULL) {
        destination->head = origin->head;
        destination->tail = origin->tail;
    } else {
        destination->tail->next = origin->head;
        destination->tail = origin->tail;
    }
    origin->head = NULL;
}

/**
 * Inserts an event in the mainloop queue, sorted by its timeout value
 * @param event A pointer to the event to insert at the end
 * @param timeout The timeout in milliseconds. If it is a positive value, it will set to NOW + timeout (relative value);
 *                if it is a negative value, it will be set to -timeout (absolute value). This last one is useful for
 *                periodic events.
 */
void eo_launch_event(EO_EVENT_P event) {

    if ((event == NULL) || (event->callback == NULL)) {
        eo_destroy_event(event);
        return;
    }
    DI();
    eo_push_event_tail(&mainloop, event);
    EI();
}

/**
 * Inserts an event in the event queue, sorted by its timeout value. This function can be called from an ISR
 * @param event A pointer to the event to insert at the end
 * @param timeout The timeout in milliseconds. If it is a positive value, it will set to NOW + timeout (relative value);
 *                if it is a negative value, it will be set to -timeout (absolute value). This last one is useful for
 *                periodic events.
 */
void eo_launch_event_ISR(EO_EVENT_P event) {

    if (event == NULL) {
        return;
    }
    eo_push_event_tail(&mainloop, event);
}


/**
 * Destroys an event, reclaiming its memory and returning it to the events heap.
 * @param event The even to destroy
 */
void eo_destroy_event(EO_EVENT_P event) {
    if (event == NULL) {
        return;
    }
    event->next = NULL;
    DI();
    eo_push_event_head(&fast_heap, event);
    EI();
}

/**
 * Creates an empty event from the events heap. This function must be used only from
 * ISR functions. Its advantage is that it doesn't use malloc, but extracts the event
 * from a linked list of recycled events. When one of these events is freed,
 * it is returned to that linked list. This makes this function not only very fast,
 * but also O(0). The inconvenient is that the events must have a fixed size,
 * defined during initialization, so they must have as much extra space as
 * the biggest block needed by the ISRs.
 *
 * @param callback The function to call when this event is dispatched
 * @param type The event type
 * @return EO_EVENT_P The new event, or NULL if memory is exhausted
 */

EO_EVENT_P eo_new_event_ISR(const EO_CALLBACK_P callback, const uint16_t type) {

    EO_EVENT_P new_event;

    new_event = eo_pop_event_head(&fast_heap);
    if (new_event) {
        new_event->callback = callback;
        new_event->type = type;
        new_event->entry_point = 0;
        new_event->data = NULL;
    }
    return new_event;
}

/**
 * Creates an empty event from the events heap. This function must not be used from
 * ISR functions. Its advantage is that it doesn't use malloc, but extracts the event
 * from a linked list of recycled events. When one of these events is freed,
 * it is returned to that linked list. This makes this function not only very fast,
 * but also O(0). The inconvenient is that the events must have a fixed size,
 * defined during initialization, so they must have as much extra space as
 * the biggest block needed by the ISRs.
 *
 * Since this is needed for AWAIT and YIELD, if the pool is exhausted, eo_no_memory()
 * will be called, which should "do something" like reseting the board, or freezing
 * it after showing a message if we are debugging.
 *
 * @param callback The function to call when this event is dispatched
 * @param type The event type
 * @return EO_EVENT_P The new event.
 */

EO_EVENT_P __crust_not_null__ eo_new_event(const uint16_t type) {

    EO_EVENT_P new_event;

    DI();
    new_event = eo_pop_event_head(&fast_heap);
    EI();
    if (new_event) {
        memset((void *)new_event, 0, sizeof(struct _eo_event));
        new_event->type = type;
        return new_event;
    }
    eo_no_memory();
    __crust_disable__
}

/**
 * Returns a block of memory from the events heap. Useful for replacing 'malloc'. The main
 * advantages are that it is much faster, and, being based on pages of fixed size, it
 * completely avoids external fragmentation. The main inconvenient is that, being of fixed
 * size, it can be complex to store data bigger than a single page.
 * This function must not be called from an ISR.
 *
 * @return a pointer to a memory block as big as the _eo_event structure, or NULL if there
 *         are no more blocks available
 */
void *eo_request_memory_block(void) {
    void *block;

    DI();
    block = (void*) eo_pop_event_head(&fast_heap); __crust_no_warning__;
    EI();
    return block;
}

/**
 * Frees a memory block obtained with eo_request_memory_block, returning it to the
 * events heap and making it available again for other functions. This function must
 * not be called from an ISR.
 *
 * @param block A pointer to a memory block obtained with eo_request_memory_block
 */

void eo_free_memory_block(void *block) {
    if (block == NULL) {
        return;
    }
    DI();
    eo_push_event_head(&fast_heap, (EO_EVENT_P) block);
    EI();
}

/**
 * Adds a timeout event. It will be emmited after the specified timeout.
 *
 * @param msg an event of EO_EVENT_TYPE_TIMEOUT. The valid params are in 'timeout' struct:
 *                timeout: the timeout in microseconds. Can be a relative or absolute value. On event dispatch, it
 *                         will contain the absolute epoch.
 *                now: when this event is returned, it will contain the actual time epoch when it was launched
 *                flags: can be any of these:
 *                      EO_TIMEOUT_RELATIVE: the timeout is relative to the current time, so this event will be
 *                                           dispatched at NOW+timeout epoch.
 *                      EO_TIMEOUT_ABSOLUTE: the timeout is an absolute epoch
 *                      EO_TIMEOUT_ROUND_TO_PERIOD: only valid if EO_TIMEOUT_RELATIVE. The epoch in which this event
 *                                                  will be dispatched will be rounded to the period value. Thus, if
 *                                                  the period is one second, it will be dispatched not after one
 *                                                  precise second, but at the begining of the next second. Thus, if
 *                                                  timeout is one second (1000000 us) and now it is epoch 78 seconds
 *                                                  and 563 milliseconds, it will be dispatched at epoch 79 seconds
 *                                                  exactly. This allows to synchronize several periodic events, ensuring
 *                                                  that the uC is woke up the minimum number of times.
 */
void eo_add_timeout(EO_EVENT_P __crust_not_null__ event) {
    uint64_t now = eo_get_now();

    if (!(event->timeout.flags & EO_TIMEOUT_ABSOLUTE)) {
        if (event->timeout.flags & EO_TIMEOUT_ROUND_TO_PERIOD) {
            event->timeout.timeout += now - (now % event->timeout.timeout);
        } else {
            event->timeout.timeout += now;
        }
    }
    if (event->timeout.timeout <= now) {
        eo_launch_event(event);
        return;
    }
    eo_push_event_sorted(&timeout_queue, event);
    eo_set_timeout(timeout_queue.head->timeout.timeout);
}

/**
 * Makes a task to wait for a sync queue. When the sync queue is triggered by another task,
 * all the tasks waiting will continue running.
 *
 * @param event The waiting event. It contains the data to return to the ASYNC task.
 * @param queue The sync queue.
 */
void eo_wait_for(EO_EVENT_P __crust_not_null__ event, EO_QUEUE_T *queue) {
    DI();
    eo_push_event_tail(queue, event);
    EI();
}

/**
 * Triggers a sync queue. When a task does this, all the tasks waiting in that sync
 * queue will continue running.
 *
 * @param queue The sync queue.
 */
void eo_trigger(EO_QUEUE_T *queue) {
    DI();
    eo_mix_queues(&mainloop, queue);
    EI();
}

/**
 * Triggers a sync queue. When a task does this, all the tasks waiting in that sync
 * queue will continue running. This version can only be called from ISR.
 *
 * @param queue The sync queue.
 */
void eo_trigger_ISR(EO_QUEUE_T *queue) {
    eo_mix_queues(&mainloop, queue);
}
