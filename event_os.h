/*
 * Copyright 2020 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _EVENT_OS_H_
#define _EVENT_OS_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {  // only need to export C interface if
              // used by C++ source code
#endif

#include "crust.h"

/**
 * This value specifies how many event memory blocks will be created and placed in the fast heap.
 * The number of events must be as high as possible, but always taking into account that some
 * space must be left for the stack and the global and static variables.
 */

#define FAST_HEAP_SIZE 29

/** List of event answer types. It contains only the types that can be set in an answer from an async
    function, not the types set in a request (because the requests can share the type without problem;
    but the answers must have different types to ensure that a single function can accept ABSOLUTELY ANY
    KIND of answers, even if it is an antipattern).

    The requests types should be defined in the headers as #defines
 */
enum eo_event_answer_type {
    EO_EVENT_TYPE_NONE = 0xC000,
    EO_EVENT_TYPE_ASYNC_TASK,
    EO_EVENT_TYPE_TIMEOUT,
    EO_EVENT_TYPE_SERIAL_END,
    EO_EVENT_TYPE_SERIAL_RECEIVED_INTERNAL_STRING,
    EO_EVENT_TYPE_I2C_OK,
    EO_EVENT_TYPE_I2C_ERROR,
};

enum eo_timeout_flags {
    EO_TIMEOUT_RELATIVE = 0,
    EO_TIMEOUT_ABSOLUTE = 1,
    EO_TIMEOUT_ROUND_TO_PERIOD = 2
};

/**
    This structure defines an event object. Events can be generated from interrupts or higher-priority
    functions, and stored in the queue to be processed later. Also they can be programmed to be launched
    after an specific timeout.

    To reduce the struct size while, at the same time, minimize the code size, the order of the fields should
    be:

    double
    uint64_t
    float
    uint32_t
    pointers
    uint16_t
    enum
    uint8_t
    bool

    this order ensures that the struct is packed and, at the same time, the elements are aligned, no matter if the
    CPU is 16 or 32 bits.
*/
struct _eo_event {

    struct _eo_event *next; //* Pointer to the next event in the queue, or NULL if this is the last event in the queue.

    void (*callback)(struct _eo_event *); //* Function to call when this event has to be processed.

    struct _eo_event *data; //* Pointer to data to keep between calls. Filled by the macros, don't use
    uint16_t entry_point; //* Entry point for the asynchronous call. Filled by the macros, don't use

    /** Event type. When an event is sent through the event queue (because it is an answer to a previous petition, or
        it is the data received in an ISR), values from 0xC000 to 0xFFFF (49152 to 65535) are reserved for eventOS
        and defined in @eo_event_answer_type, while values from 0x0000 to 0xBFFF (0 to 49151) are available to the
        programmer.

        When doing a direct call, like, for example, when calling @eo_serial_send(), this limitation isn't needed, so
        the value is free. Thus it can have the event type and, optionally, several flags if desired by the programmer.
    */

    uint16_t type;

    union {
        struct {
            uint64_t timeout;
            uint64_t now;
            enum eo_timeout_flags flags;
        } timeout;

        struct {
            uint8_t buffer[21];
            uint8_t size;
        } serial_tx_mini; // used to send bytes through the serial port from a small buffer, stored in the event itself
        struct {
            uint8_t *buffer;
            uint16_t size;
        } serial_tx_external; // used to send bytes through the serial port from an external, big buffer
        struct {
            uint8_t *buffer;
        } serial_tx_external_string; // used to send external zero-ended strings
        struct {
            uint8_t buffer[22];
        } serial_tx_internal_string; // used to send internal zero-ended strings

        struct {
            uint8_t buffer[22];
        } serial_rx_internal_string; // emited whenever a block has been received in the serial port

        struct {
            uint8_t size;
            uint8_t address;
            uint8_t buffer[20];
        } i2c_internal_write; // only send a small block of data
        struct {
            uint8_t wr_size;
            uint8_t rd_size;
            uint8_t address;
            uint8_t buffer[19];
        } i2c_internal_write_read; // write, then read, small blocks of data. The write data starts at buffer[0]; the read data is stored at buffer[wr_size]
        struct {
            uint8_t *external_block;
            int external_size;
            uint8_t internal_size;
            uint8_t address;
            uint8_t buffer[16];
        } i2c_external_write; // write two blocks, one internal, one external, to the I2C. Useful for flash memories.
        struct {
            uint8_t *external_block;
            int external_size;
            uint8_t internal_size;
            uint8_t address;
            uint8_t buffer[16];
        } i2c_external_write_read; // write an internal block, reads an external block. Useful for flash memories.
    };
};

#define EO_EVENT_DATA_SIZE 22

typedef struct _eo_event EO_EVENT;
typedef __crust__ struct _eo_event* EO_EVENT_P;
typedef void (*EO_CALLBACK_P)(EO_EVENT_P __crust_not_null__);

/**
    This structure contains a queue of event objects. Methods for inserting and removing
    are available, both to work in FIFO and LIFO ways.

    An important implementation detail is that if the queue is empty, only *head* points
    to NULL; *tail* can point to garbage.
 */
struct _EO_QUEUE_T {
    EO_EVENT_P head;
    EO_EVENT_P tail;
};

typedef struct _EO_QUEUE_T EO_QUEUE_T;

/** These macros emulates YIELD and asynchronous functions in C
    There are two conditions:
      * It is forbiden to use switch/case inside the asynchronous code
      * The asynchronous function must have EO_CALLBACK_P format (only one parameter of type EO_EVENT_P, no return value)

    An example function would be:

    EO_ASYNC(demo_async_fnc, event) { // begins an async function. The first parameter is the function name; the second,
                                      // the name of the parameter received by the function

        // variables and other elements to define and/or run every time the function is called

        EO_BEGIN_ASYNC(event); // the semicolon is mandatory. The parameter is the name of the parameter passed to the function
        // asynchronous code here, putting 'EO_YIELD(function, event);' to give control to the main loop. The semicolon is mandatory.
        //                                                              'event' must be a different event than the one passed from the caller.
        EO_END_ASYNC(); // the semicolon is mandatory.
    }

    To call an asynchronous function AFUNC, use EO_RUN_ASYNC_TASK(AFUNC), or use EO_YIELD(AFUNC, event_p) to call it from
    another asynchronous function. It is important to remember that *event_p* must be a new event created specifically to
    call AFUNC; it must not be the one received by the current function. In fact, to preserve the return data, the event
    received in the function must be stored in the *data* filed of the new event. This is done automagically by the EO_YIELD()
    macro.

    The AFUNC function will do its job and store the result in the event received from the caller. When it finished and
    is ready to return the value, it will do so with EO_RETURN_ASYNC, passing the event received originally from the
    caller, and a new event type.

    It is possible to just wait an arbitrary number of microseconds using EO_AWAIT(timeout). This call will consume one
    event from the events heap.
*/

#define EO_ASYNC(FNAME) void FNAME(EO_EVENT_P data) { static EO_CALLBACK_P _current_function_p_ = FNAME;

#define EO_BEGIN_ASYNC() switch((data && (data->callback == _current_function_p_)) ? data->entry_point : 0) {default:

#define EO_END_ASYNC() eo_launch_event(data); }}

#define EO_EXIT_ASYNC() eo_launch_event(data); return;

#define EO_YIELD(FUNCTION, EVENT_P) {EVENT_P->data = data;\
                                     EVENT_P->callback = _current_function_p_;\
                                     EVENT_P->entry_point = __LINE__;\
                                     FUNCTION(EVENT_P);\
                                     return;\
                                     case __LINE__:\
                                     EVENT_P = data;\
                                     data = EVENT_P->data;}

#define EO_AWAIT(TIMEOUT) {data = eo_build_timeout(TIMEOUT, data);\
                           data->entry_point = __LINE__;\
                           data->callback = _current_function_p_;\
                           eo_add_timeout(data);\
                           return;\
                           case __LINE__:\
                           data = eo_recover_data(data);}

#define EO_RUN_ASYNC_TASK(TASK) TASK(NULL)
#define EO_RUN_ASYNC_TASK_P(TASK, PARAMS) TASK(PARAMS)

/** This #define is used for periodic events */

#define EO_PERIODIC(EVENT, PERIOD) (-((EVENT)->timeout + (PERIOD)))

// Inner API
// These functions should not be used directly

EO_EVENT_P __crust_not_null__ eo_build_timeout(int64_t timeout, EO_EVENT_P data);
EO_EVENT_P eo_recover_data(EO_EVENT_P event);
void eo_timeout_manage(EO_EVENT_P __crust_not_null__ event);

// Public API

void eo_init(__crust_not_null__ void(*cb)(void));

// Queues
void eo_init_queue(EO_QUEUE_T *queue);
void eo_push_event_head(EO_QUEUE_T *queue, EO_EVENT_P event);
void eo_push_event_tail(EO_QUEUE_T *queue, EO_EVENT_P event);
EO_EVENT_P eo_peek_event_head(EO_QUEUE_T *queue);
EO_EVENT_P eo_pop_event_head(EO_QUEUE_T *queue);
void eo_push_event_sorted(EO_QUEUE_T *queue, EO_EVENT_P __crust_not_null__ event);
void eo_mix_queues(EO_QUEUE_T *destination, EO_QUEUE_T *origin);

// Event management
EO_EVENT_P __crust_not_null__ eo_new_event(const uint16_t type);
EO_EVENT_P eo_new_event_ISR(const EO_CALLBACK_P callback, const uint16_t type);
void eo_destroy_event(EO_EVENT_P event);

// Dynamic memory
void *eo_request_memory_block(void);
void eo_free_memory_block(void *block);

// Launching tasks and events
void eo_launch_event(EO_EVENT_P event);
void eo_launch_event_ISR(EO_EVENT_P event);

void eo_add_timeout(EO_EVENT_P __crust_not_null__ event);
void eo_wait_for(EO_EVENT_P __crust_not_null__ event, EO_QUEUE_T *queue);
void eo_trigger(EO_QUEUE_T *queue);
void eo_trigger_ISR(EO_QUEUE_T *queue);

#ifdef __cplusplus
}
#endif

#include "nonportable.h"

#endif //_EVENT_OS_H
